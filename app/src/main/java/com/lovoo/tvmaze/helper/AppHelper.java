package com.lovoo.tvmaze.helper;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by Kosh on 13 May 2017, 9:59 PM
 */

public class AppHelper {

    public static boolean isN() {return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N;}

    @Nullable public static Fragment getFragmentByTag(@NonNull FragmentManager fragmentManager, @NonNull String tag) {
        return fragmentManager.findFragmentByTag(tag);
    }

    @Nullable static Activity getActivity(@Nullable Context content) {
        if (content == null) return null;
        else if (content instanceof Activity) return (Activity) content;
        else if (content instanceof ContextWrapper) return getActivity(((ContextWrapper) content).getBaseContext());
        return null;
    }

}
