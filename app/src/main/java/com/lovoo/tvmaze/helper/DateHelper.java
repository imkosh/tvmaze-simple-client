package com.lovoo.tvmaze.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import lombok.NonNull;

/**
 * Created by Kosh on 19 May 2017, 9:36 PM
 */

public class DateHelper {

    @NonNull public static String getToday() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
    }

    @NonNull public static String getAirTime(@NonNull String airTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm", Locale.ENGLISH);
        try {
            Date date = dateFormat.parse(airTime);
            if (date != null) {
                dateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
                dateFormat.setTimeZone(TimeZone.getDefault());
                return dateFormat.format(date);
            }
        } catch (ParseException ignored) {}
        return "";
    }

    @NonNull public static Date getTodayDate() {
        return new java.sql.Date(System.currentTimeMillis());
    }

}
