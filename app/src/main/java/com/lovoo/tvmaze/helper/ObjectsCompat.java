package com.lovoo.tvmaze.helper;

import io.reactivex.annotations.Nullable;

/**
 * Created by Kosh on 19 May 2017, 9:34 PM
 */

public class ObjectsCompat {

    public static boolean nonNull(@Nullable Object obj) {
        return obj != null;
    }
}
