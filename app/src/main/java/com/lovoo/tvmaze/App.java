package com.lovoo.tvmaze;

import android.app.Application;
import android.support.annotation.NonNull;

import com.lovoo.tvmaze.dao.model.Models;

import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;

/**
 * Created by Kosh on 19 May 2017, 7:28 PM
 */

public class App extends Application {

    private static App instance;
    private ReactiveEntityStore<Persistable> dataStore;

    @Override public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @NonNull public static App getInstance() {
        return instance;
    }

    @NonNull public ReactiveEntityStore<Persistable> getDataStore() {
        if (dataStore == null) {
            DatabaseSource source = new DatabaseSource(this, Models.DEFAULT, 1);
            source.setLoggingEnabled(BuildConfig.DEBUG);
            Configuration configuration = source.getConfiguration();
            dataStore = ReactiveSupport.toReactiveStore(new EntityDataStore<Persistable>(configuration));
        }
        return dataStore;
    }
}
