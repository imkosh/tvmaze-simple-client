package com.lovoo.tvmaze.ui.modules.welcome;

import com.lovoo.tvmaze.ui.base.mvp.BaseMvp;

/**
 * Created by Kosh on 19 May 2017, 9:31 PM
 */

interface WelcomeMvp {

    interface View extends BaseMvp.FAView {
        void onSuccessfullySaved();
    }

    interface Presenter {
        void onStartLoadingData();
    }
}
