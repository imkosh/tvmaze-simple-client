package com.lovoo.tvmaze.ui.modules.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.lovoo.tvmaze.R;
import com.lovoo.tvmaze.dao.model.AbstractTvShow;
import com.lovoo.tvmaze.ui.base.BaseActivity;
import com.lovoo.tvmaze.ui.base.mvp.presenter.BasePresenter;
import com.lovoo.tvmaze.ui.modules.welcome.WelcomeActivity;

import net.grandcentrix.thirtyinch.TiPresenter;

public class MainActivity extends BaseActivity {

    public static void startActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override protected int layout() {
        return R.layout.activity_main;
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            if (AbstractTvShow.getCount(new java.sql.Date(System.currentTimeMillis())) == 0) {
                WelcomeActivity.startActivity(this);
                finish();
            }
        }
    }

    @NonNull @Override public TiPresenter providePresenter() {
        return new BasePresenter();
    }
}
