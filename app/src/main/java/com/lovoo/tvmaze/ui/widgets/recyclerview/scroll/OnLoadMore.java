package com.lovoo.tvmaze.ui.widgets.recyclerview.scroll;

import android.support.annotation.Nullable;

import com.lovoo.tvmaze.ui.base.mvp.BaseMvp;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter public class OnLoadMore<P> extends InfiniteScroll {

    private final BaseMvp.PaginationListener<P> presenter;
    @Nullable private P parameter;

    public OnLoadMore(BaseMvp.PaginationListener<P> presenter, @Nullable P parameter) {
        super();
        this.presenter = presenter;
        this.parameter = parameter;
    }

    @Override protected void onLoadMore(int page, int previousTotal) {
        super.onLoadMore(page, previousTotal);
        if (presenter != null) {
            presenter.setPreviousTotal(previousTotal);
            presenter.onLoadMore(page, parameter);
        }
    }
}