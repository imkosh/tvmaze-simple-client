package com.lovoo.tvmaze.ui.adapter.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lovoo.tvmaze.R;
import com.lovoo.tvmaze.dao.model.Image;
import com.lovoo.tvmaze.dao.model.Show;
import com.lovoo.tvmaze.dao.model.TvShow;
import com.lovoo.tvmaze.helper.DateHelper;
import com.lovoo.tvmaze.ui.widgets.recyclerview.BaseRecyclerAdapter;
import com.lovoo.tvmaze.ui.widgets.recyclerview.BaseViewHolder;

import butterknife.BindString;
import butterknife.BindView;

/**
 * Created by Kosh on 20 May 2017, 2:25 PM
 */

public class TvShowViewHolder extends BaseViewHolder<TvShow> {

    @BindView(R.id.image) AppCompatImageView image;
    @BindView(R.id.name) AppCompatTextView name;
    @BindView(R.id.airtime) AppCompatTextView airtime;
    @BindView(R.id.duration) AppCompatTextView duration;
    @BindView(R.id.number) AppCompatTextView number;
    @BindString(R.string.minutes) String minutes;
    private final Context parentContext;

    private TvShowViewHolder(@NonNull View itemView, @Nullable BaseRecyclerAdapter adapter, @NonNull Context parentContext) {
        super(itemView, adapter);
        this.parentContext = parentContext;
    }

    public static TvShowViewHolder newInstance(@NonNull ViewGroup parent, @NonNull BaseRecyclerAdapter adapter) {
        return new TvShowViewHolder(getView(parent, R.layout.tv_show_row_item), adapter, parent.getContext());
    }

    @Override public void bind(@NonNull TvShow tvShow) {
        if (tvShow.getNumber() > 0) {
            number.setText(String.format("%s (%s)", tvShow.getSeason(), tvShow.getNumber()));
            number.setVisibility(View.VISIBLE);
        } else {
            number.setVisibility(View.GONE);
        }
        Show show = tvShow.getShow();
        if (show != null) {
            name.setText(String.format("%s(%s)", show.getName(), tvShow.getName()));
            if (!TextUtils.isEmpty(tvShow.getAirtime())) {
                airtime.setText(DateHelper.getAirTime(tvShow.getAirtime()));
                airtime.setVisibility(View.VISIBLE);
            } else {
                airtime.setVisibility(View.GONE);
            }
            duration.setText(String.format("%s %s", tvShow.getRuntime(), minutes));
            Image imageModel = show.getImage();
            if (imageModel != null) {
                String imageUrl = imageModel.getMedium() == null ? imageModel.getOriginal() : imageModel.getMedium();
                if (imageUrl != null) {
                    Glide.with(parentContext != null ? parentContext : image.getContext())
                            .load(imageUrl)
                            .dontAnimate()
                            .fitCenter()
                            .fallback(AppCompatResources.getDrawable(itemView.getContext(), R.drawable.ic_broken_image))
                            .placeholder(AppCompatResources.getDrawable(itemView.getContext(), R.drawable.ic_image))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(image);
                }
            } else {
                setBrokenImage();
            }
        } else {
            setBrokenImage();
        }
    }

    private void setBrokenImage() {
        Glide.clear(image);
        image.setImageResource(R.drawable.ic_broken_image);
    }
}
