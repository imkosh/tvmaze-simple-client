package com.lovoo.tvmaze.ui.modules.welcome;

import com.lovoo.tvmaze.dao.model.AbstractTvShow;
import com.lovoo.tvmaze.helper.DateHelper;
import com.lovoo.tvmaze.helper.ObjectsCompat;
import com.lovoo.tvmaze.helper.RxHelper;
import com.lovoo.tvmaze.provider.rest.RestProvider;
import com.lovoo.tvmaze.ui.base.mvp.presenter.BasePresenter;

/**
 * Created by Kosh on 19 May 2017, 9:32 PM
 */

public class WelcomePresenter extends BasePresenter<WelcomeMvp.View> implements WelcomeMvp.Presenter {

    @Override public void onStartLoadingData() {
        makeRestCall(RxHelper.getObservable(RestProvider.getService()
                        .getTvShows("US", DateHelper.getToday()))
                        .filter(ObjectsCompat::nonNull)
                        .flatMap(AbstractTvShow::save),
                tvShows -> sendToView(WelcomeMvp.View::onSuccessfullySaved));
    }
}
