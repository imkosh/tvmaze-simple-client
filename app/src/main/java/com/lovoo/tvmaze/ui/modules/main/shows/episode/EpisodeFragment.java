package com.lovoo.tvmaze.ui.modules.main.shows.episode;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.transition.TransitionManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lovoo.tvmaze.R;
import com.lovoo.tvmaze.dao.model.Image;
import com.lovoo.tvmaze.dao.model.Show;
import com.lovoo.tvmaze.dao.model.TvShow;
import com.lovoo.tvmaze.helper.AppHelper;
import com.lovoo.tvmaze.ui.adapter.SimpleRoundedAdapter;
import com.lovoo.tvmaze.ui.base.BaseFragment;
import com.lovoo.tvmaze.ui.widgets.recyclerview.DynamicRecyclerView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Kosh on 20 May 2017, 8:53 PM
 */

public class EpisodeFragment extends BaseFragment<EpisodeMvp.View, EpisodePresenter> implements EpisodeMvp.View {

    public static final String TAG = EpisodeFragment.class.getSimpleName();

    @BindView(R.id.image) AppCompatImageView image;
    @BindView(R.id.number) AppCompatTextView number;
    @BindView(R.id.name) AppCompatTextView name;
    @BindView(R.id.description) AppCompatTextView description;
    @BindView(R.id.days) DynamicRecyclerView day;
    @BindView(R.id.appbar) AppBarLayout appbar;
    @BindView(R.id.toggle) View toggle;
    @Nullable @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.rating) AppCompatRatingBar rating;
    @BindView(R.id.status) AppCompatTextView status;
    @BindView(R.id.network) AppCompatTextView network;
    @BindView(R.id.showTitle) AppCompatTextView showTitle;
    @BindView(R.id.showSummary) AppCompatTextView showSummary;
    @BindView(R.id.bottomSheet) FrameLayout bottomSheet;
    @BindView(R.id.info) FloatingActionButton info;
    private BottomSheetBehavior bottomSheetBehavior;

    public static EpisodeFragment newInstance(int id) {
        Bundle args = new Bundle();
        EpisodeFragment fragment = new EpisodeFragment();
        args.putInt(EpisodeMvp.ID_KEY, id);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick(R.id.toggle) void onExpandDescription() {
        TransitionManager.beginDelayedTransition((ViewGroup) toggle);
        if (description.getMaxLines() == 2) {
            description.setMaxLines(20);//some random number, not applicable for production tho.
            name.setCompoundDrawablesWithIntrinsicBounds(null, null,
                    AppCompatResources.getDrawable(getContext(), R.drawable.ic_arrow_drop_up), null);
        } else {
            description.setMaxLines(2);
            name.setCompoundDrawablesWithIntrinsicBounds(null, null,
                    AppCompatResources.getDrawable(getContext(), R.drawable.ic_arrow_drop_down), null);
        }
    }

    @OnClick(R.id.info) void onShowInfo() {
        info.hide();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        initShowDetails();
    }

    @OnClick(R.id.showTitle) void onMinimizeBottomSheet() {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    @Override protected int fragmentLayout() {
        return R.layout.tv_show_details_layout;
    }

    @Override protected void onFragmentCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initDrawables();
        ViewCompat.setNestedScrollingEnabled(day, false);
        if (toolbar != null) {
            toolbar.setNavigationIcon(AppCompatResources.getDrawable(getContext(), R.drawable.ic_back));
            appbar.addOnOffsetChangedListener(this);
            toolbar.setNavigationOnClickListener(v -> getFragmentManager()
                    .popBackStack(TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE));
        }
        if (savedInstanceState == null) {
            getPresenter().onActivityCreated(getArguments());
        } else {
            onInit(getPresenter().getTvShow());
        }
        bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.bottomSheet));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    info.show();
                }
            }

            @Override public void onSlide(@NonNull View bottomSheet, float slideOffset) {}
        });
    }

    @Override public void onOffsetChanged(AppBarLayout abl, int verticalOffset) {
        float ratioOfCollapse = (-verticalOffset / (float) abl.getTotalScrollRange());
        if (getPresenter().getTvShow() != null && toolbar != null) {
            if (ratioOfCollapse < 1) {
                toolbar.setTitle("");
            } else {
                toolbar.setTitle(getPresenter().getTvShow().getShow().getName());
            }
        }
    }

    @Override public void onInit(@Nullable TvShow tvShow) {
        if (tvShow == null) {
            Toast.makeText(getContext(), R.string.general_error_message, Toast.LENGTH_SHORT).show();
            return;
        }
        Show show = tvShow.getShow();
        ViewCompat.setTransitionName(image, ViewCompat.getTransitionName(image) + "-" + tvShow.getId());
        if (show != null) {
            name.setText(show.getName());
            number.setText(String.valueOf(tvShow.getNumber()));
            if (AppHelper.isN()) {
                description.setText(Html.fromHtml(show.getSummary(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                description.setText(Html.fromHtml(show.getSummary()));
            }
            loadImage(show);
            if (show.getSchedule() != null) day.setAdapter(new SimpleRoundedAdapter(show.getSchedule().getDays()));
            if (show.getRating() != null) {
                rating.setProgress((int) show.getRating().getAverage());
            }
            status.setText(show.getStatus());
            network.setText(show.getNetwork() != null ? show.getNetwork().getName() : "N/A");
        }
    }

    @NonNull @Override public EpisodePresenter providePresenter() {
        return new EpisodePresenter();
    }

    @Override public void onDestroyView() {
        if (toolbar != null) appbar.removeOnOffsetChangedListener(this);
        super.onDestroyView();
    }

    private void loadImage(Show show) {
        Image imageModel = show.getImage();
        if (imageModel != null) {
            String imageUrl = imageModel.getOriginal() == null ? imageModel.getMedium() : imageModel.getOriginal();
            if (imageUrl != null) {
                Glide.with(this)
                        .load(imageUrl)
                        .fallback(AppCompatResources.getDrawable(getContext(), R.drawable.ic_broken_image))
                        .placeholder(AppCompatResources.getDrawable(getContext(), R.drawable.ic_image))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(image);
            }
        }
    }

    private void initDrawables() {
        if (description.getMaxLines() == 2) {
            name.setCompoundDrawablesWithIntrinsicBounds(null, null,
                    AppCompatResources.getDrawable(getContext(), R.drawable.ic_arrow_drop_down), null); //#vectorDrawablePreL 😣
        } else {
            name.setCompoundDrawablesWithIntrinsicBounds(null, null,
                    AppCompatResources.getDrawable(getContext(), R.drawable.ic_arrow_drop_up), null);//#vectorDrawablePreL 😣
        }
        showTitle.setCompoundDrawablesWithIntrinsicBounds(null, null,
                AppCompatResources.getDrawable(getContext(), R.drawable.ic_arrow_drop_down), null); //#vectorDrawablePreL 😣
    }

    private void initShowDetails() {
        TvShow tvShow = getPresenter().getTvShow();
        if (tvShow != null) {
            showTitle.setText(tvShow.getName());
            if (AppHelper.isN()) {
                showSummary.setText(Html.fromHtml(tvShow.getSummary(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                showSummary.setText(Html.fromHtml(tvShow.getSummary()));
            }
        }
    }
}
