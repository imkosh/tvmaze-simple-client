package com.lovoo.tvmaze.ui.modules.main.shows;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.lovoo.tvmaze.dao.model.AbstractTvShow;
import com.lovoo.tvmaze.dao.model.TvShow;
import com.lovoo.tvmaze.helper.DateHelper;
import com.lovoo.tvmaze.helper.RxHelper;
import com.lovoo.tvmaze.ui.base.mvp.BaseMvp;
import com.lovoo.tvmaze.ui.base.mvp.presenter.BasePresenter;

import java.util.ArrayList;
import java.util.Date;

import lombok.Getter;

/**
 * Created by Kosh on 20 May 2017, 4:35 AM
 */

@Getter public class TvShowsPresenter extends BasePresenter<TvShowsMvp.View> implements TvShowsMvp.Presenter {

    private ArrayList<TvShow> shows = new ArrayList<>();
    private final int count;
    private int page;
    private int previousTotal;

    TvShowsPresenter() {
        this.count = TvShow.getCount(DateHelper.getTodayDate());
    }

    @Override public void onItemClick(int position, View v, TvShow item) {
        if (getView() != null) getView().onItemClicked(item.getId());
    }

    @Override public int getCurrentPage() {
        return page;
    }

    @Override public int getPreviousTotal() {
        return previousTotal;
    }

    @Override public void setCurrentPage(int page) {
        this.page = page;
    }

    @Override public void setPreviousTotal(int previousTotal) {
        this.previousTotal = previousTotal;
    }

    @Override public void onLoadMore(int page, @Nullable Date parameter) {
        if (page == 0) {
            previousTotal = 0;
        }
        if (previousTotal > count) {
            sendToView(BaseMvp.FAView::hideProgress);
            return;
        }
        Log.e("Count", "Count" + page + " " + previousTotal);
        setCurrentPage(page);
        manageSubscription(RxHelper.getObservable(AbstractTvShow.getShows(previousTotal, parameter))
                .doOnSubscribe(disposable -> onSubscribed())
                .subscribe(tvShows -> sendToView(v -> v.onNotifyAdapter(tvShows, page)), this::onError));
    }
}
