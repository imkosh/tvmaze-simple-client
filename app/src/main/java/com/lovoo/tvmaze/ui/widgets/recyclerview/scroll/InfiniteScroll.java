package com.lovoo.tvmaze.ui.widgets.recyclerview.scroll;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Kosh on 8/2/2015. copyrights are reserved @
 */
@SuppressWarnings("FieldCanBeLocal") public abstract class InfiniteScroll extends RecyclerView.OnScrollListener {
    private int previousTotal = 0;
    private boolean loading = true;
    private final int visibleThreshold = 2;
    private int firstVisibleItem;
    private int visibleItemCount;
    private int totalItemCount;
    private int current_page = 0;
    private RecyclerViewPositionHelper mRecyclerViewHelper;

    InfiniteScroll() {}

    @Override public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(recyclerView);
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mRecyclerViewHelper.getItemCount();
        firstVisibleItem = mRecyclerViewHelper.findFirstVisibleItemPosition();
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            current_page++;
            onLoadMore(current_page, previousTotal);
            loading = true;
        }
    }

    void onLoadMore(int page, int previousTotal) {}

    public void reset() {
        this.previousTotal = 0;
        this.loading = true;
        this.current_page = 0;
    }

    public void setData(int page, int previousTotal) {
        this.current_page = page;
        this.previousTotal = previousTotal;
        this.loading = true;

    }
}

