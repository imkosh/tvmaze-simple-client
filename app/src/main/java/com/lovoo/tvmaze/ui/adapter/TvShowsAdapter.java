package com.lovoo.tvmaze.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.lovoo.tvmaze.dao.model.TvShow;
import com.lovoo.tvmaze.ui.adapter.viewholder.TvShowViewHolder;
import com.lovoo.tvmaze.ui.widgets.recyclerview.BaseRecyclerAdapter;
import com.lovoo.tvmaze.ui.widgets.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by Kosh on 20 May 2017, 2:34 PM
 */

public class TvShowsAdapter extends BaseRecyclerAdapter<TvShow, TvShowViewHolder, BaseViewHolder.OnItemClickListener<TvShow>> {

    public TvShowsAdapter(@NonNull List<TvShow> data, @Nullable BaseViewHolder.OnItemClickListener<TvShow> listener) {
        super(data, listener);
    }

    @Override protected TvShowViewHolder viewHolder(ViewGroup parent, int viewType) {
        return TvShowViewHolder.newInstance(parent, this);
    }

    @Override protected void onBindView(TvShowViewHolder holder, int position) {
        holder.bind(getItem(position));
    }
}
