package com.lovoo.tvmaze.ui.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.lovoo.tvmaze.BuildConfig;
import com.lovoo.tvmaze.R;
import com.lovoo.tvmaze.helper.AppHelper;
import com.lovoo.tvmaze.helper.ViewHelper;
import com.lovoo.tvmaze.ui.base.mvp.BaseMvp;
import com.lovoo.tvmaze.ui.base.mvp.presenter.BasePresenter;
import com.lovoo.tvmaze.ui.modules.main.shows.episode.EpisodeFragment;

import net.grandcentrix.thirtyinch.TiActivity;

import butterknife.ButterKnife;
import icepick.Icepick;

/**
 * Created by Kosh on 24 May 2016, 8:48 PM
 */

public abstract class BaseActivity<V extends BaseMvp.FAView, P extends BasePresenter<V>> extends TiActivity<P, V> implements
        BaseMvp.FAView {

    private Toast toast;

    @LayoutRes protected abstract int layout();

    @Override protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (layout() != 0) {
            setContentView(layout());
            ButterKnife.bind(this);
        }
        Icepick.setDebug(BuildConfig.DEBUG);
        if (savedInstanceState != null && !savedInstanceState.isEmpty()) {
            Icepick.restoreInstanceState(this, savedInstanceState);
        }
    }

    @Override public void showMessage(@StringRes int titleRes, @StringRes int msgRes) {
        showMessage(getString(titleRes), getString(msgRes));
    }

    @Override public void showMessage(@NonNull String titleRes, @NonNull String msgRes) {
        hideProgress();
        if (toast != null) toast.cancel();
        toast = titleRes.equals(getString(R.string.error))
                ? Toast.makeText(getApplicationContext(), msgRes, Toast.LENGTH_LONG)
                : Toast.makeText(getApplicationContext(), msgRes, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override public void showErrorMessage(@NonNull String msgRes) {
        showMessage(getString(R.string.error), msgRes);
    }

    @Override public void showProgress() {

    }

    @Override public void hideProgress() {}

    @SuppressLint("RestrictedApi") @Override public void onBackPressed() {
        if (!ViewHelper.isLandscape(getResources()) && !ViewHelper.isTablet(this)) {
            Fragment fragment = AppHelper.getFragmentByTag(getSupportFragmentManager(), EpisodeFragment.TAG);
            if (fragment != null && !fragment.isHidden()) {
                getSupportFragmentManager().popBackStack(EpisodeFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }
}
