package com.lovoo.tvmaze.ui.modules.main.shows;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.lovoo.tvmaze.R;
import com.lovoo.tvmaze.dao.model.TvShow;
import com.lovoo.tvmaze.helper.ViewHelper;
import com.lovoo.tvmaze.ui.adapter.TvShowsAdapter;
import com.lovoo.tvmaze.ui.base.BaseFragment;
import com.lovoo.tvmaze.ui.modules.main.shows.episode.EpisodeFragment;
import com.lovoo.tvmaze.ui.widgets.StateLayout;
import com.lovoo.tvmaze.ui.widgets.recyclerview.DynamicRecyclerView;
import com.lovoo.tvmaze.ui.widgets.recyclerview.scroll.OnLoadMore;

import java.util.Date;
import java.util.List;

import butterknife.BindView;

/**
 * A placeholder fragment containing a simple view.
 */
public class TvShowsFragment extends BaseFragment<TvShowsMvp.View, TvShowsPresenter> implements TvShowsMvp.View {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.stateLayout) StateLayout stateLayout;
    @BindView(R.id.recyclerView) DynamicRecyclerView recyclerView;
    @BindView(R.id.refresh) SwipeRefreshLayout refresh;

    private OnLoadMore<Date> loadMore;
    private TvShowsAdapter adapter;

    @Override public void onNotifyAdapter(@Nullable List<TvShow> items, int page) {
        hideProgress();
        if (items == null) {
            adapter.clear();
        } else if (!items.isEmpty()) {
            if (page > 0) adapter.addItems(items);
            else adapter.insertItems(items);
        }
    }

    @NonNull @Override public OnLoadMore<Date> loadMore() {
        if (loadMore == null) {
            loadMore = new OnLoadMore<>(getPresenter(), new java.sql.Date(System.currentTimeMillis()));
        }
        return loadMore;
    }

    @Override public void onItemClicked(int id) {
        getFragmentManager().popBackStack(EpisodeFragment.TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (ViewHelper.isTablet(getActivity()) && ViewHelper.isLandscape(getResources())) {
            transaction.replace(R.id.innerContainer, EpisodeFragment.newInstance(id), EpisodeFragment.TAG);
        } else {
            transaction.replace(R.id.innerContainer, EpisodeFragment.newInstance(id), EpisodeFragment.TAG)
                    .addToBackStack(EpisodeFragment.TAG);
        }
        transaction.commit();
    }

    @Override protected int fragmentLayout() {
        return R.layout.shows_layout;
    }

    @Override protected void onFragmentCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        toolbar.setTitle(R.string.shows);
        adapter = new TvShowsAdapter(getPresenter().getShows(), getPresenter());
        loadMore().setData(getPresenter().getCurrentPage(), getPresenter().getPreviousTotal());
        if (savedInstanceState == null) {
            refresh();
        }
        recyclerView.addDivider();
        recyclerView.setEmptyView(stateLayout, refresh);
        refresh.setOnRefreshListener(this::refresh);
        stateLayout.setOnReloadListener(v -> refresh());
        recyclerView.addOnScrollListener(loadMore());
        recyclerView.setAdapter(adapter);
    }

    @NonNull @Override public TvShowsPresenter providePresenter() {
        return new TvShowsPresenter();
    }

    @Override public void showProgress() {
        if (!refresh.isRefreshing()) stateLayout.showProgress();
    }

    @Override public void hideProgress() {
        stateLayout.hideProgress();
        refresh.setRefreshing(false);
        stateLayout.showReload(adapter.getItemCount());
    }

    @Override public void onDestroyView() {
        recyclerView.removeOnScrollListener(loadMore());
        super.onDestroyView();
    }

    private void refresh() {
        loadMore().reset();
        getPresenter().onLoadMore(0, loadMore().getParameter());
    }
}
