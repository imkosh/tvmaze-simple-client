package com.lovoo.tvmaze.ui.base.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.lovoo.tvmaze.R;
import com.lovoo.tvmaze.helper.RxHelper;
import com.lovoo.tvmaze.ui.base.mvp.BaseMvp;

import net.grandcentrix.thirtyinch.TiPresenter;
import net.grandcentrix.thirtyinch.rx2.RxTiPresenterDisposableHandler;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class BasePresenter<V extends BaseMvp.FAView> extends TiPresenter<V> implements BaseMvp.FAPresenter {
    private final RxTiPresenterDisposableHandler subscriptionHandler = new RxTiPresenterDisposableHandler(this);

    @Override public void manageSubscription(@Nullable Disposable... subscription) {
        if (subscription != null) {
            subscriptionHandler.manageDisposables(subscription);
        }
    }

    @Override public void onSubscribed() {
        sendToView(BaseMvp.FAView::showProgress);
    }

    @Override public void onError(@NonNull Throwable throwable) {
        throwable.printStackTrace();
        sendToView(v -> v.showMessage(R.string.error, getPrettifiedErrorMessage(throwable)));
    }

    @Override public <T> void makeRestCall(@NonNull Observable<T> observable, @NonNull Consumer<T> onNext) {
        manageSubscription(RxHelper.getObservable(observable)
                .doOnSubscribe(disposable -> onSubscribed())
                .subscribe(onNext, this::onError, () -> sendToView(BaseMvp.FAView::hideProgress))
        );
    }

    @StringRes private int getPrettifiedErrorMessage(@Nullable Throwable throwable) {
        int resId = R.string.network_error;
        if (throwable instanceof HttpException) {
            resId = R.string.network_error;
        } else if (throwable instanceof IOException) {
            resId = R.string.request_error;
        } else if (throwable instanceof TimeoutException) {
            resId = R.string.unexpected_error;
        }
        return resId;
    }

}
