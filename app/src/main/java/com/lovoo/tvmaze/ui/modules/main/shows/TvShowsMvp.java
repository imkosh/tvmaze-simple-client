package com.lovoo.tvmaze.ui.modules.main.shows;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.lovoo.tvmaze.dao.model.TvShow;
import com.lovoo.tvmaze.ui.base.mvp.BaseMvp;
import com.lovoo.tvmaze.ui.widgets.recyclerview.BaseViewHolder;
import com.lovoo.tvmaze.ui.widgets.recyclerview.scroll.OnLoadMore;

import java.util.Date;
import java.util.List;

/**
 * Created by Kosh on 20 May 2017, 4:33 AM
 */

interface TvShowsMvp {

    interface View extends BaseMvp.FAView {
        void onNotifyAdapter(@Nullable List<TvShow> items, int page);

        @NonNull OnLoadMore<Date> loadMore();

        void onItemClicked(int id);
    }

    interface Presenter extends BaseMvp.PaginationListener<Date>, BaseViewHolder.OnItemClickListener<TvShow> {

    }
}
