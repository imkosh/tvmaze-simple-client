package com.lovoo.tvmaze.ui.modules.main.shows.episode;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.lovoo.tvmaze.dao.model.TvShow;
import com.lovoo.tvmaze.ui.base.mvp.presenter.BasePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

import static com.lovoo.tvmaze.ui.modules.main.shows.episode.EpisodeMvp.ID_KEY;

/**
 * Created by Kosh on 20 May 2017, 9:00 PM
 */

@Getter public class EpisodePresenter extends BasePresenter<EpisodeMvp.View> implements EpisodeMvp.Presenter {
    private TvShow tvShow;

    @Override public void onActivityCreated(@Nullable Bundle arguments) {
        if (arguments != null) {
            int id = arguments.getInt(ID_KEY, -1);
            if (id > 0) {
                manageSubscription(TvShow.getTvShow(id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(tvShow1 -> sendToView(view -> view.onInit(this.tvShow = tvShow1))));
                return;
            }
        }
        sendToView(view -> view.onInit(null));
    }
}
