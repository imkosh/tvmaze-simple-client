package com.lovoo.tvmaze.ui.modules.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.AutoTransition;
import android.support.transition.TransitionManager;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.LinearLayout;

import com.lovoo.tvmaze.R;
import com.lovoo.tvmaze.ui.base.BaseActivity;
import com.lovoo.tvmaze.ui.modules.main.MainActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Kosh on 19 May 2017, 9:36 PM
 */

public class WelcomeActivity extends BaseActivity<WelcomeMvp.View, WelcomePresenter> implements WelcomeMvp.View {


    @BindView(R.id.progressLayout) LinearLayout progressLayout;
    @BindView(R.id.reload) AppCompatButton reload;
    @BindView(R.id.progressLayoutHolder) LinearLayout progressLayoutHolder;

    public static void startActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, WelcomeActivity.class));
    }

    @OnClick(R.id.reload) void onReload() {
        getPresenter().onStartLoadingData();
    }

    @Override protected int layout() {
        return R.layout.welcome_layout;
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getPresenter().onStartLoadingData();
        }
    }

    @NonNull @Override public WelcomePresenter providePresenter() {
        return new WelcomePresenter();
    }

    @Override public void onSuccessfullySaved() {
        progressLayout.setVisibility(View.GONE);
        TransitionManager.beginDelayedTransition(progressLayoutHolder, new AutoTransition());
        MainActivity.startActivity(this);
        finish();
    }

    @Override public void showProgress() {
        super.showProgress();
        reload.setVisibility(View.GONE);
        progressLayout.setVisibility(View.VISIBLE);
        TransitionManager.beginDelayedTransition(progressLayoutHolder);
    }

    @Override public void showMessage(int titleRes, int msgRes) {
        super.showMessage(titleRes, msgRes);
        progressLayout.setVisibility(View.GONE);
        reload.setVisibility(View.VISIBLE);
        TransitionManager.beginDelayedTransition(progressLayoutHolder);
    }
}
