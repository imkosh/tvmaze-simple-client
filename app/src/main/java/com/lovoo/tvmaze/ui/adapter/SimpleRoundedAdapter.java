package com.lovoo.tvmaze.ui.adapter;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.lovoo.tvmaze.ui.adapter.viewholder.SimpleRoundViewHolder;
import com.lovoo.tvmaze.ui.widgets.recyclerview.BaseRecyclerAdapter;
import com.lovoo.tvmaze.ui.widgets.recyclerview.BaseViewHolder;

import java.util.List;

/**
 * Created by Kosh on 21 May 2017, 12:46 AM
 */

public class SimpleRoundedAdapter extends BaseRecyclerAdapter<String, SimpleRoundViewHolder, BaseViewHolder.OnItemClickListener<String>> {
    public SimpleRoundedAdapter(@NonNull List<String> data) {
        super(data);
    }

    @Override protected SimpleRoundViewHolder viewHolder(ViewGroup parent, int viewType) {
        return SimpleRoundViewHolder.newInstance(parent);
    }

    @Override protected void onBindView(SimpleRoundViewHolder holder, int position) {
        holder.bind(getItem(position));
    }
}
