package com.lovoo.tvmaze.ui.widgets.recyclerview;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by Kosh on 17 May 2016, 7:13 PM
 */
public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder implements View.OnClickListener {

    public interface OnItemClickListener<T> {
        void onItemClick(int position, View v, T item);
    }

    @Nullable private final BaseRecyclerAdapter adapter;

    protected static View getView(@NonNull ViewGroup parent, @LayoutRes int layoutRes) {
        return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
    }

    protected BaseViewHolder(@NonNull View itemView) {
        this(itemView, null);
    }

    protected BaseViewHolder(@NonNull View itemView, @Nullable BaseRecyclerAdapter adapter) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.adapter = adapter;
        itemView.setOnClickListener(this);
    }

    @SuppressWarnings("unchecked") @Override public void onClick(View v) {
        if (adapter != null && adapter.getListener() != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION && position < adapter.getItemCount())
                adapter.getListener().onItemClick(position, v, adapter.getItem(position));
        }
    }

    public abstract void bind(@NonNull T t);

}
