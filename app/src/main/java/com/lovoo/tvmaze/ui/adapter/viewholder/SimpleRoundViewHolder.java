package com.lovoo.tvmaze.ui.adapter.viewholder;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.ViewGroup;

import com.lovoo.tvmaze.R;
import com.lovoo.tvmaze.helper.ColorGenerator;
import com.lovoo.tvmaze.ui.widgets.recyclerview.BaseViewHolder;

import butterknife.BindView;

/**
 * Created by Kosh on 21 May 2017, 12:38 AM
 */

public class SimpleRoundViewHolder extends BaseViewHolder<String> {

    @BindView(R.id.textView) AppCompatTextView textView;

    private SimpleRoundViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    @NonNull public static SimpleRoundViewHolder newInstance(@NonNull ViewGroup parent) {
        return new SimpleRoundViewHolder(getView(parent, R.layout.day_row_item));
    }

    @Override public void bind(@NonNull String s) {
        textView.setText(s);
        Drawable background = textView.getBackground();
        if (background instanceof ShapeDrawable) {
            ShapeDrawable shapeDrawable = (ShapeDrawable) background;
            shapeDrawable.getPaint().setColor(ColorGenerator.MATERIAL.getColor(s));
        } else if (background instanceof GradientDrawable) {//will always fall here
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            gradientDrawable.setColor(ColorGenerator.MATERIAL.getColor(s));
        }
    }
}
