package com.lovoo.tvmaze.ui.modules.main.shows.episode;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;

import com.lovoo.tvmaze.dao.model.TvShow;
import com.lovoo.tvmaze.ui.base.mvp.BaseMvp;

/**
 * Created by Kosh on 20 May 2017, 8:58 PM
 */

interface EpisodeMvp {

    String ID_KEY = "id";

    interface View extends BaseMvp.FAView, AppBarLayout.OnOffsetChangedListener {
        void onInit(@Nullable TvShow tvShow);
    }

    interface Presenter {
        void onActivityCreated(@Nullable Bundle intent);
    }
}
