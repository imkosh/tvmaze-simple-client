package com.lovoo.tvmaze.ui.base.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import net.grandcentrix.thirtyinch.TiView;
import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Kosh on 25 May 2016, 9:09 PM
 */

public interface BaseMvp {

    interface FAView extends TiView {

        @CallOnMainThread void showProgress();

        @CallOnMainThread void hideProgress();

        @CallOnMainThread void showMessage(@StringRes int titleRes, @StringRes int msgRes);

        @CallOnMainThread void showMessage(@NonNull String titleRes, @NonNull String msgRes);

        @CallOnMainThread void showErrorMessage(@NonNull String msgRes);
    }

    interface FAPresenter {
        void manageSubscription(@Nullable Disposable... subscription);

        void onSubscribed();

        void onError(@NonNull Throwable throwable);

        <T> void makeRestCall(@NonNull Observable<T> observable, @NonNull Consumer<T> onNext);
    }

    interface PaginationListener<P> {
        int getCurrentPage();

        int getPreviousTotal();

        void setCurrentPage(int page);

        void setPreviousTotal(int previousTotal);

        void onLoadMore(int page, @Nullable P parameter);
    }
}
