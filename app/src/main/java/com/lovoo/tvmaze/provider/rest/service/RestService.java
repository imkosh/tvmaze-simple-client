package com.lovoo.tvmaze.provider.rest.service;

import android.support.annotation.NonNull;

import com.lovoo.tvmaze.dao.model.TvShow;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Kosh on 19 May 2017, 7:59 PM
 */

public interface RestService {

    @GET("schedule") Observable<List<TvShow>> getTvShows(@Query("country") @NonNull String country, @Query("date") @NonNull String date);

}
