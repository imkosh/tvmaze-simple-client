package com.lovoo.tvmaze.provider.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lovoo.tvmaze.BuildConfig;
import com.lovoo.tvmaze.provider.rest.service.RestService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kosh on 19 May 2017, 8:02 PM
 */

public class RestProvider {

    private static final Gson GSON = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    private static Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.REST_URL)
                .client(provideHttpClient())
                .addConverterFactory(GsonConverterFactory.create(GSON))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private static OkHttpClient provideHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    public static RestService getService() {
        return provideRetrofit().create(RestService.class);
    }

}
