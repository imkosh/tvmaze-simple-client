package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Kosh on 19 May 2017, 7:15 PM
 */

@Entity @Getter @Setter @NoArgsConstructor public abstract class AbstractCountry implements Parcelable {
    @Generated @Key int id;
    String name;
    String code;
    String timezone;

    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.code);
        dest.writeString(this.timezone);
    }

    protected AbstractCountry(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.code = in.readString();
        this.timezone = in.readString();
    }

    public static final Parcelable.Creator<Country> CREATOR = new Parcelable.Creator<Country>() {
        @Override public Country createFromParcel(Parcel source) {return new Country(source);}

        @Override public Country[] newArray(int size) {return new Country[size];}
    };
}
