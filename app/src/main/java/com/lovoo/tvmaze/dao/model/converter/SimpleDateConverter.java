package com.lovoo.tvmaze.dao.model.converter;

import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.requery.Converter;

/**
 * Created by Kosh on 20 May 2017, 1:08 PM
 */

public class SimpleDateConverter implements Converter<Date, String> {
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    @Override public Class<Date> getMappedType() {
        return Date.class;
    }

    @Override public Class<String> getPersistedType() {
        return String.class;
    }

    @Nullable @Override public Integer getPersistedSize() {
        return null;
    }

    @Override public String convertToPersisted(Date value) {
        if (value != null) {
            return simpleDateFormat.format(value);
        }
        return null;
    }

    @Override public Date convertToMapped(Class<? extends Date> type, @Nullable String value) {
        try {
            return simpleDateFormat.parse(value);
        } catch (ParseException ignored) {}
        return null;
    }
}
