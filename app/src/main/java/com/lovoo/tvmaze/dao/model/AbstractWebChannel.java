package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Kosh on 19 May 2017, 9:51 PM
 */

@Getter @Setter @NoArgsConstructor @Entity public abstract class AbstractWebChannel implements Parcelable {

    @SerializedName("ignore") @Generated @Key long id;
    String name;
    @OneToOne @ForeignKey Country country;


    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeParcelable(this.country, flags);
    }

    protected AbstractWebChannel(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.country = in.readParcelable(Country.class.getClassLoader());
    }

    public static final Parcelable.Creator<WebChannel> CREATOR = new Parcelable.Creator<WebChannel>() {
        @Override public WebChannel createFromParcel(Parcel source) {return new WebChannel(source);}

        @Override public WebChannel[] newArray(int size) {return new WebChannel[size];}
    };
}
