package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Kosh on 19 May 2017, 7:15 PM
 */

@Entity @Getter @Setter @NoArgsConstructor public abstract class AbstractRating implements Parcelable {
    @Generated @Key int id;
    double average;

    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeDouble(this.average);
    }

    protected AbstractRating(Parcel in) {
        this.id = in.readInt();
        this.average = in.readDouble();
    }

    public static final Creator<Rating> CREATOR = new Creator<Rating>() {
        @Override public Rating createFromParcel(Parcel source) {return new Rating(source);}

        @Override public Rating[] newArray(int size) {return new Rating[size];}
    };
}
