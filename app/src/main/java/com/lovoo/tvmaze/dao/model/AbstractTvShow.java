package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.lovoo.tvmaze.App;
import com.lovoo.tvmaze.dao.model.converter.SimpleDateConverter;
import com.lovoo.tvmaze.helper.RxHelper;

import java.util.Date;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Key;
import io.requery.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import static com.lovoo.tvmaze.dao.model.TvShow.AIRTIME;
import static com.lovoo.tvmaze.dao.model.TvShow.AIR_DATE;
import static com.lovoo.tvmaze.dao.model.TvShow.ID;

/**
 * Created by Kosh on 19 May 2017, 7:14 PM
 */

@Getter @Setter @NoArgsConstructor @Entity public abstract class AbstractTvShow implements Parcelable {

    @Key int id;
    @ForeignKey @OneToOne Show show;
    @SerializedName("airdate") @Convert(SimpleDateConverter.class) Date airDate;
    @SerializedName("airstamp") Date airStamp;
    String url;
    String name;
    String airtime;
    String summary;
    int runtime;
    int season;
    int number;

    public static int getCount(@NonNull Date date) {
        return App.getInstance().getDataStore()
                .count(TvShow.class)
                .where(AIR_DATE.eq(date))
                .get()
                .value();
    }

    public static Maybe<TvShow> getTvShow(int id) {
        return App.getInstance().getDataStore()
                .select(TvShow.class)
                .where(ID.eq(id))
                .get()
                .maybe();
    }

    public static Observable<Iterable<TvShow>> save(@NonNull List<TvShow> shows) {
        return RxHelper.getObservable(App.getInstance().getDataStore().delete(TvShow.class)
                .get()
                .single()
                .toObservable()
                .flatMap(integer -> App.getInstance().getDataStore().insert(shows).toObservable()));
    }

    public static Observable<List<TvShow>> getShows(int page, @NonNull Date date) {
        return App.getInstance().getDataStore().select(TvShow.class)
                .where(AIR_DATE.eq(date))
                .orderBy(AIR_DATE.asc(), AIRTIME.asc())
                .limit(30)
                .offset(page)
                .get()
                .observable()
                .toList()
                .toObservable();
    }

    public static Observable<List<TvShow>> getTags() {
        return App.getInstance().getDataStore()
                .select(TvShow.class)
                .distinct()
                .join(Show.class)
                .on(TvShow.SHOW_ID.eq(Show.ID))
                .groupBy(Show.TYPE.lower())
                .orderBy(Show.TYPE.desc())
                .get()
                .observable()
                .toList()
                .toObservable();
    }

    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeParcelable(this.show, flags);
        dest.writeLong(this.airDate != null ? this.airDate.getTime() : -1);
        dest.writeLong(this.airStamp != null ? this.airStamp.getTime() : -1);
        dest.writeString(this.url);
        dest.writeString(this.name);
        dest.writeString(this.airtime);
        dest.writeString(this.summary);
        dest.writeInt(this.runtime);
        dest.writeInt(this.season);
        dest.writeInt(this.number);
    }

    protected AbstractTvShow(Parcel in) {
        this.id = in.readInt();
        this.show = in.readParcelable(Show.class.getClassLoader());
        long tmpAirDate = in.readLong();
        this.airDate = tmpAirDate == -1 ? null : new Date(tmpAirDate);
        long tmpAirStamp = in.readLong();
        this.airStamp = tmpAirStamp == -1 ? null : new Date(tmpAirStamp);
        this.url = in.readString();
        this.name = in.readString();
        this.airtime = in.readString();
        this.summary = in.readString();
        this.runtime = in.readInt();
        this.season = in.readInt();
        this.number = in.readInt();
    }

    public static final Creator<TvShow> CREATOR = new Creator<TvShow>() {
        @Override public TvShow createFromParcel(Parcel source) {return new TvShow(source);}

        @Override public TvShow[] newArray(int size) {return new TvShow[size];}
    };
}
