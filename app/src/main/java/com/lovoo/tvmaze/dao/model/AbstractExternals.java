package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Kosh on 19 May 2017, 7:15 PM
 */

@Entity @Getter @Setter @NoArgsConstructor public abstract class AbstractExternals implements Parcelable {

    @Generated @Key int id;
    @SerializedName("tvrage") long tvRage;
    @SerializedName("thetvdb") long theTvDb;
    String imdb;

    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeLong(this.tvRage);
        dest.writeLong(this.theTvDb);
        dest.writeString(this.imdb);
    }

    protected AbstractExternals(Parcel in) {
        this.id = in.readInt();
        this.tvRage = in.readLong();
        this.theTvDb = in.readLong();
        this.imdb = in.readString();
    }

    public static final Parcelable.Creator<Externals> CREATOR = new Parcelable.Creator<Externals>() {
        @Override public Externals createFromParcel(Parcel source) {return new Externals(source);}

        @Override public Externals[] newArray(int size) {return new Externals[size];}
    };
}
