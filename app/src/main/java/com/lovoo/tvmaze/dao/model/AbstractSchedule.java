package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.lovoo.tvmaze.dao.StringListModel;
import com.lovoo.tvmaze.dao.model.converter.StringListConverter;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Kosh on 19 May 2017, 7:15 PM
 */

@Entity @Getter @Setter @NoArgsConstructor public abstract class AbstractSchedule implements Parcelable {
    @Generated @Key int id;
    @Column(name = "name_column") String time;
    @Convert(StringListConverter.class) StringListModel days;

    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.time);
        dest.writeList(this.days);
    }

    protected AbstractSchedule(Parcel in) {
        this.id = in.readInt();
        this.time = in.readString();
        this.days = new StringListModel();
        in.readList(this.days, this.days.getClass().getClassLoader());
    }

    public static final Parcelable.Creator<Schedule> CREATOR = new Parcelable.Creator<Schedule>() {
        @Override public Schedule createFromParcel(Parcel source) {return new Schedule(source);}

        @Override public Schedule[] newArray(int size) {return new Schedule[size];}
    };
}
