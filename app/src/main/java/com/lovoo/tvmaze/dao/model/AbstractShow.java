package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.lovoo.tvmaze.App;
import com.lovoo.tvmaze.dao.StringListModel;
import com.lovoo.tvmaze.dao.model.converter.SimpleDateConverter;
import com.lovoo.tvmaze.dao.model.converter.StringListConverter;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Kosh on 19 May 2017, 7:15 PM
 */

@Entity @Getter @Setter @NoArgsConstructor public abstract class AbstractShow implements Parcelable {
    @SerializedName("ignore") @Generated @Key int id;
    @SerializedName("id") int showId;
    String url;
    String name;
    String type;
    @Column(name = "language_column") String language;
    String status;
    @Convert(SimpleDateConverter.class) Date premiered;
    String summary;
    int runtime;
    int weight;
    int updated;
    @Convert(StringListConverter.class) StringListModel genres;
    @OneToOne @ForeignKey WebChannel webChannel;
    @OneToOne @ForeignKey Schedule schedule;
    @OneToOne @ForeignKey Rating rating;
    @OneToOne @ForeignKey Network network;
    @OneToOne @ForeignKey Externals externals;
    @OneToOne @ForeignKey Image image;

    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.showId);
        dest.writeString(this.url);
        dest.writeString(this.name);
        dest.writeString(this.type);
        dest.writeString(this.language);
        dest.writeString(this.status);
        dest.writeLong(this.premiered != null ? this.premiered.getTime() : -1);
        dest.writeString(this.summary);
        dest.writeInt(this.runtime);
        dest.writeInt(this.weight);
        dest.writeInt(this.updated);
        dest.writeList(this.genres);
        dest.writeParcelable(this.webChannel, flags);
        dest.writeParcelable(this.schedule, flags);
        dest.writeParcelable(this.rating, flags);
        dest.writeParcelable(this.network, flags);
        dest.writeParcelable(this.externals, flags);
        dest.writeParcelable(this.image, flags);
    }

    protected AbstractShow(Parcel in) {
        this.id = in.readInt();
        this.showId = in.readInt();
        this.url = in.readString();
        this.name = in.readString();
        this.type = in.readString();
        this.language = in.readString();
        this.status = in.readString();
        long tmpPremiered = in.readLong();
        this.premiered = tmpPremiered == -1 ? null : new Date(tmpPremiered);
        this.summary = in.readString();
        this.runtime = in.readInt();
        this.weight = in.readInt();
        this.updated = in.readInt();
        this.genres = new StringListModel();
        in.readList(this.genres, this.genres.getClass().getClassLoader());
        this.webChannel = in.readParcelable(WebChannel.class.getClassLoader());
        this.schedule = in.readParcelable(Schedule.class.getClassLoader());
        this.rating = in.readParcelable(Rating.class.getClassLoader());
        this.network = in.readParcelable(Network.class.getClassLoader());
        this.externals = in.readParcelable(Externals.class.getClassLoader());
        this.image = in.readParcelable(Image.class.getClassLoader());
    }

    public static final Creator<Show> CREATOR = new Creator<Show>() {
        @Override public Show createFromParcel(Parcel source) {return new Show(source);}

        @Override public Show[] newArray(int size) {return new Show[size];}
    };
}
