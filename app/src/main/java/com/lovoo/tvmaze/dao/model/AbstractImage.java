package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Kosh on 19 May 2017, 7:15 PM
 */

@Entity @Getter @Setter @NoArgsConstructor public abstract class AbstractImage implements Parcelable {
    @Generated @Key int key;
    String medium;
    String original;

    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.key);
        dest.writeString(this.medium);
        dest.writeString(this.original);
    }

    protected AbstractImage(Parcel in) {
        this.key = in.readInt();
        this.medium = in.readString();
        this.original = in.readString();
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        @Override public Image createFromParcel(Parcel source) {return new Image(source);}

        @Override public Image[] newArray(int size) {return new Image[size];}
    };
}
