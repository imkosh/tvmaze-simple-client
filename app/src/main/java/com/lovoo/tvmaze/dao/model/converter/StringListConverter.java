package com.lovoo.tvmaze.dao.model.converter;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.lovoo.tvmaze.dao.StringListModel;

import io.requery.Converter;

/**
 * Created by Kosh on 19 May 2017, 7:43 PM
 */

public class StringListConverter implements Converter<StringListModel, String> {
    private final Gson gson = new Gson();

    @Override public Class<StringListModel> getMappedType() {
        return StringListModel.class;
    }

    @Override public Class<String> getPersistedType() {
        return String.class;
    }

    @Nullable @Override public Integer getPersistedSize() {
        return null;
    }

    @Override public String convertToPersisted(StringListModel value) {
        if (value != null) return gson.toJson(value);
        return null;
    }

    @Override public StringListModel convertToMapped(Class<? extends StringListModel> type, @Nullable String value) {
        if (value != null) return gson.fromJson(value, StringListModel.class);
        return null;
    }
}
