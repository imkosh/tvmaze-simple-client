package com.lovoo.tvmaze.dao.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Kosh on 19 May 2017, 7:15 PM
 */

@Entity @Getter @Setter @NoArgsConstructor public abstract class AbstractNetwork implements Parcelable {
    @SerializedName("ignored")/* we don't need the Id so you generated ID to handle deletion & updates properly*/
    @Generated @Key int id;
    @OneToOne @ForeignKey Country country;
    String name;

    @Override public int describeContents() { return 0; }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeParcelable(this.country, flags);
        dest.writeString(this.name);
    }

    protected AbstractNetwork(Parcel in) {
        this.id = in.readInt();
        this.country = in.readParcelable(Country.class.getClassLoader());
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Network> CREATOR = new Parcelable.Creator<Network>() {
        @Override public Network createFromParcel(Parcel source) {return new Network(source);}

        @Override public Network[] newArray(int size) {return new Network[size];}
    };
}
